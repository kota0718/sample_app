class RemoveJobFromPackage < ActiveRecord::Migration
  def change
    remove_column :packages, :job, :id
  end
end
